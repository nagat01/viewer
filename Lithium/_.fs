open FStap open __ open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "Lithium"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "Lithium: 多目的の画像ビューワー"; STAThread>] SET_ENG_CULTURE
open System.Collections.Generic
open System.IO


// vals
let images    = Queue()
let imageCur  = None'
let targetDir = None'

// ui
let wnd = Window' "Lithium"

let grid      = Grid()  $ wnd $ addColsStar [0.5; 0.5] 
let  dpImg    = DockPanel()  $ grid $ "efe".bg
let   spSource= HStackPanel() $ dpImg.top
let   btnDir  = Button'("分類元フォルダ","fff") $ spSource
let   btnClear= Button'("クリア","fff")        $ spSource
let   btnNext = Button'("次の画像へ","fff")     $ spSource
let   img     = Image() $ dpImg 
let  dpDirs   = DockPanel()  $ grid.addTo(col=1) $ "efe".bg
let   lblStat = Label'() $ dpDirs.top
let   spDirs  = HStackPanel() $ dpDirs.top
let   btnImg  = Button'("パネル開閉","fff")     $ spDirs
let   btnDirs = Button'("分類先フォルダ","fff") $ spDirs
let   tbxMkDir= TextBox'() $ w 100 $ spDirs
let   ug      = UGrid() $ dpDirs


// funcs
let countImage() =
  wnd.Title <- 
    match imageCur with
    | Some' file -> sf "%s(%d枚)" file images.len
    | _ -> "画像なし"

let showImage() =
  images.tryDeq |%| fun file -> 
    img.Source <- (file $ imageCur |> mkImageSource).v 
  countImage()

let moveImage dir =
  try 
    let file = imageCur.v 
    File.Move(file, dir +/ file.name)
    showImage()
  with _ ->
    msg "移動失敗!!"

let showDirs (dir:s) =
  catedAll <*- 0
  let dirs = dir.dirs
  let n = (dirs.len.f ** 0.5).ceili
  ug.clear
  ug $ cols n $ rows n |> ig
  for dir in dirs do
    let btn = DirectoryButton(dir, moveImage) $ ug
    catedAll <*- catedAll +. btn.nFile
  countImage()


// events
btnDir =+ {
  let! files = openDir "source_directory" exts 
  files |%|* images
  showImage()
  }

btnClear =+ { 
  images.Clear()
  countImage()
  }

btnNext =+ { showImage() }

btnImg =+ {
  let b = grid.cd.len = 1
  grid.cd.Clear()
  grid |> addColsStar (If b [0.5; 0.5] [1.])
  }

btnDirs =+ {
  let! dir = selDir "target_directory" 
  dir $ showDirs |>* targetDir
  }

tbxMkDir.enter =+? {
   let! dir = targetDir 
   (dir +/ tbxMkDir.Text).mkDir
   showDirs dir }

catedAll >< catedCur =+ {
  lblStat <*- sf "分類済み:全体%d, 今回%d" catedAll.v catedCur.v }

wnd.run