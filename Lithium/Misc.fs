﻿[<AutoOpen>]
module __.Misc
open FStap open System open System.Windows open System.Windows.Controls
open System.Net
open System.Windows.Shapes
open FSharp.Data


let exts = "jpg jpeg bmp png tiff gif ico"

let catedAll = sh 0
let catedCur = sh 0

let getDroppedImages (e:DragEventArgs) =
  let urls0 = seq {
    let html = e.Data.GetData "HTML Format" :?> s
    if html.ok then
      for attr in (HtmlDocument.Parse html).Descendants "img" |> Seq.choose(HtmlNode.tryGetAttribute "src") do
        yield attr.Value() }
  e.Data.GetData DataFormats.FileDrop |> tryCast |? urls0

let saveImage dir (path:s) = try' {
  let src = path
  let dest = dir +/ src.split("/").last
  use wc = new WebClient()
  wc.DownloadFile (src, dest) }


// control
type DirectoryButton (dir:s, moveImage) as __ =
  inherit Border()
  let mutable _nFile = 0 
  let grid     = Grid()          $ __ $ allowDrop
  let rect     = Rectangle()     $ grid $ fill "4f00" $ haLeft
  let dp       = DockPanel()          $ grid
  let  lblName = Label' dir.name $ dp.top
  let  lblNum  = Label' ()       $ dp.top
  let update n =
    catedCur <*- (catedCur +. n)
    catedAll <*- (catedAll +. n)
  let countFile() = 
    let count = dir.files("").len
    lblNum <*- count
    if count > 0 then 
      imRetry 10 100 (fun _ -> grid.rw.ok) {
      rect.w <- grid.rw * (log count.f / log 10000.)
      rect.h <- grid.rh }
    _nFile <- count
  do
    __ $ "ff8".bg $ bdr 0.5 "000" $ corner 2. $ mgn1 1 $ stretch |> ig
    countFile()

    grid.lClick'() =+ { 
      moveImage dir 
      countFile() 
      update 1
     }

    grid.Drop => fun e ->
      let paths = getDroppedImages e
      for path in paths do
        saveImage dir path
      update paths.len
      countFile()
  member __.nFile = _nFile
        
    


