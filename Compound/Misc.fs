[<AutoOpen>]
module Misc
open FStap open System open System.Windows open System.Windows.Controls 
open System.Windows.Shapes


// main
let Ico = ImageProvider 20.

let gtip = Controls.tip
guide |> "cfff".bg

let wnd = GlassWindow "Compound" $ gtip "" $ restorePosSz


// control
type IconButton (ico:Image, ttl) as __ = 
  inherit HStackPanel()    
  let  _ = ico        $ __ 
  let  _ = Label' ttl $ __ 
  do __ $ mgn1 0 $ onEnter "dff".bg $ onLeave "fff".bg |> ig
  member __.e = __.mdown


type GageSlider(mi, ma, ini, _w, ?bg, ?fg, ?lever, ?volume) as __ =
  inherit Grid()
  let bg     = bg     |? "fff"
  let fg     = fg     |? "000"
  let volume = volume |? "fff"
  let lever  = lever  |? "000"
  let mutable _v = sh ini 
  let cnv         = Canvas()    $ __  $ wh _w 20 $ vaCenter
  let  rectLine   = Rectangle() $ cnv $ wh _w 5 $ mvy 7.5 $ fill "fff"
  let  rectVolume = Rectangle() $ cnv $ h 5     $ mvy 7.5 $ fill volume 
  let  rectLever  = Rectangle(RadiusX=2., RadiusY=2.) $ cnv $ wh 8 20 $ tr2 -4 0 $ fill "fff" $ stroke 1 lever
  let lbl         = Label'()    $ __ $ fsz 10 $ fg.fg $ vaBottom $ haRight
  let vtox v = (v - mi) / (ma - mi) * _w |> within 0. _w
  let xtov x = (ma - mi) * (x / _w) + mi |> within mi ma
  do 
    __ $ w (_w + 10.) $ bg.bg |> ig
    _v => fun v -> 
      let x = vtox v
      rectLever |> mvx x
      if x > 0. then
        rectVolume |> w x
      lbl <*- sf "%.2f" v
  member __.v 
    with get() = _v.v 
    and  set v = _v <*- v
  member __.e =
    (__.lmdown >< __.lDrag ) 
      -% { yield xtov (MPosOn rectLine).X $ _v }

 

// func
let [<Literal>] Alpha = 0xff000000
let [<Literal>] Red   = 0x00ff0000
let [<Literal>] Green = 0x0000ff00
let [<Literal>] Blue  = 0x000000ff

let changeGamma gamma (__:Wb) = 
  let n = 2. ** gamma
  let blues  = [| for i in 0 .. 255 -> ((i.f / 256.) ** n) * 256. |> within 0. 255. |> int |]
  let greens = blues  |%> fun i -> i * 256 
  let reds   = greens |%> fun i -> i * 256
  let alphas = reds   |%> fun i -> i * 256
  let f i = 
    alphas.[(int i >>> 24) &&& 255] + 
    reds  .[(int i >>> 16) &&& 255] +
    greens.[(int i >>> 8 ) &&& 255] +
    blues .[ int i         &&& 255] 
  let pixels = __.pixels
  for i in 0 .. __.parea - 1 do
    pixels.[i] <- f pixels.[i]
  __.writeAll pixels


// init
let ini = wnd.ig

