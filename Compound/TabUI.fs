﻿module TabUI
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Input


// constant
let Colors = [
  "0fff";"fff"; "ccc"; "888"; "444"; "000"
  "006"; "066"; "060"; "660"; "600"; "606"
  "ffd"; "edc"; "fdd"; "ecd"; "fdf"; "dce"
  "ddf"; "dec"; "dff"; "ced"; "dfd"; "cde"
  ]

let sw = Stopwatch'()

// Control
type IconKeyButton(ico, ttl, key) as __ =
  inherit Grid()
  let lbl = ShadowedLabel(10., "77f",  key.s) $ __ $ vaTop $ haLeft $ zIdx 1
  let btn = IconButton(ico, ttl) $ __
  member __.e = btn.mdown >< wnd.kd key


// ui
let dp               = DockPanel()  $ lastChildFill
let  sp              = HStackPanel() $ dp.top $ "fff".bg $ zIdx 1
let   spTab          = HStackPanel() $ sp 
let    lblsTab       = [| for i in 1..9 -> Label' i $ spTab $ fsz 14 $ w 20 $ vaStretch $ ccenter $ gtip (sf "押す or %dキー: %d番目のタブのキャンバスを表示します" i i) |]
let   btnAddImage    = IconKeyButton(Ico.add,         "追加", Key.A) $ sp $ gtip "押す or Aキー: キャンバスに画像を追加します"
let   btnTransparent = IconKeyButton(Ico.transparent, "表示", Key.S) $ sp $ gtip "押す or Sキー: 画像の上のメニューの表示をオン・オフします"
let   btnClear       = IconKeyButton(Ico.clear,     "クリア", Key.D) $ sp $ gtip "押す or Dキー: キャンバス上の画像を全てクリアします"
let   btnSave        = IconKeyButton(Ico.save,        "保存", Key.F) $ sp $ gtip "押す or Fキー: キャンバスの内容を画像として保存します"
let  sliGamma        = GageSlider(-1., 1., 0., 50., bg="ddd", fg="77f") $ sp $ gtip "スライダーの移動: 表示される画像のガンマ補正をします"
let   ugColors       = UGrid(c=12,r=2) $ sp $ wh 120 20 $ gtip "押す: キャンバスの背景色を変更できます"
let    btnColors     = [| for co in Colors -> Label'() $ ugColors $ co.bg $ bdr 0.5 "ccc" $ vaStretch $ haStretch |]
let gridPage         = Grid() $ dp.bottom 
let  _               = guide  $ gridPage $ vaTop $ haRight
let  pages           = [| for i in 0 .. 8 -> CompoundCanvas i |]


// variable
let mutable selected = 1


// func
let select i =
  selected <- i
  for i in 0..8 do
    lblsTab.[i] 
    $ (If (selected = i) "eef" "fff").bg
    $ (If (selected = i) "77f" "ccc").fg 
    |> ig
  remChildren<CompoundCanvas> gridPage
  gridPage.addHead pages.[selected] 


// event
for i in 0..8 do
  let key  = parseEnum (sf "D%d" (i+1))
  let page = pages.[i]
  
  // event
  let inline filter __ = __ -? { V (selected = i) }
  let selected = lblsTab.[i].mdown >< wnd.kd key  
  selected              =+ { select i }
  filter btnAddImage    =+ { page.addImage }
  filter btnTransparent =+ { page.menuTransparent }
  filter btnClear       =+ { page.clearCanvas }
  filter btnSave        =+ { if sw.isMSec 1000 then page.saveCanvas; sw.restart }
  filter sliGamma       =+ { page.gamma <*-* sliGamma }
  filter page.gamma >< 
  selected >< loaded    =+ { sliGamma <*-* page.gamma }

for btn in btnColors do
  btn.mdown =+ { pages.[selected].setBackground btn.Background.co.str } 


// init
select 0