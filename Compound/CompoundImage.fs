[<AutoOpen>]
module CompoundPanel
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Input
open System.Windows.Media.Imaging
open System.Windows.Shapes


let pfloat x = try Double.Parse x with _ -> 0.

// constant
let mutable _zIdx = 0


type CompoundImage (tabs:Canvas rarray, gamma:f sh, ?props:s) as __ =
  inherit StackPanel()
  let mutable _path       = Def
  let mutable bmp         = (None:Wb opt)
  let mutable szImage     = Def<Sz>
  let mutable zIndex      = 0
  let mutable isFlip      = false
  let mutable pDown       = None
  let mutable vCropOffset = vec 0 0
  let mutable r32Crop     = Def
  let sp        = HStackPanel() $ __ $ discreet 0.5 $ zIdx 1 $ "fff".bg 
  let  btnOpen  = IconButton(Ico.open',   "") $ sp $ gtip "画像開く: このパネルに表示する画像を開きます"
  let  btnDef   = IconButton(Ico.restore, "") $ sp $ gtip "初期位置: このパネルの画像を初期位置・角度に戻します"
  let  btnFlip  = IconButton(Ico.flip,    "") $ sp $ gtip "左右反転: このパネルの画像を左右反転します"
  let  btnRem   = IconButton(Ico.remove,  "") $ sp $ gtip "パネル削除: このパネルを削除します。画像ファイルは削除されません"
  let cnv       = Canvas()    $ __
  let  img      = Image()     $ cnv $ gtip "左ボタンでドラッグ: 画像を移動できます\r\n右ボタンでドラッグ: 画像を拡大縮小回転できます\r\n中央ボタンでドラッグ: 画像の切り抜きができます"
  let  rectCrop = Rectangle() $ cnv $ stroke 1 "f80" $ collapsed

  let tr = 
    Transformer (
      cnv, 
      TRS, 
      scCenter  = (fun _ -> po cnv.aw 0. /. 2.), 
      rotCenter = (fun _ -> po cnv.aw (min img.aw img.ah) /. 2.) )

  let doFlip (wb:Wb) =
    let pixels = wb.pixels
    let w = wb.pw
    wb.writeRectXY wb.r32 <| fun x y ->
      pixels.[(y + 1) * w - (x + 1)]

  let arrangeBmp initPos (r32:R32) = async {
    do! r32.ok
    let! bmp = bmp
    let wb = WriteableBitmap bmp $ changeGamma gamma.v
    if isFlip then 
      doFlip wb
    if wb.pw < r32.r || wb.ph < r32.b then
      wb |>* img
    else
      CroppedBitmap (wb, r32) |>* img
    do! initPos 
    do! 100 
    po (sp.aw - img.aw) 0. /. 2. |> tr.Move
    sp.aw / img.aw |> tr.Scale }

  let openFileDialog _ = 
    openFile (sf "openFile%d" __.idx) "*" (__.openFile >> immediate)

  let rectInsideImage p0 p1 = 
    (Rect.ofPo2 p0 p1).trimedRect 0. 0. szImage.w szImage.h

  let resizeRect (fsz:Sz) (tsz:Sz) (rect:Rect) = opt {
    let! bmp = bmp
    let rect = rect *. (tsz /. fsz)
    return rect.trimedR32 0. 0. bmp.pw.f bmp.ph.f  }
  
  let bringTop (z:_ opt) =
    z |%| fun z -> _zIdx <- z
    zIndex <- &_zIdx +=! 1
    __ |> zIdx zIndex

  let rotateZoom (e:DragEventArg) =
    tr.Rotate e.vx
    tr.Zoom (e.vy * 0.01)

  do
    bringTop None

    for i in 0..8 do
      let btn = 
        Label'(i+1) $ sp $ fsz 14 $ w 20 $ "fff".bg $ bdr 0.5 "000" 
        $ vaStretch $ ccenter $ gtip (sf "押す:画像を%d番目のキャンバスに移動します" i)
      btn.lclick =+ { __ $ __.canvas.rem |>* tabs.[i] }
      [ for canvas in tabs -> canvas.menter -%% canvas ]
      |> Observable.concat => fun canvas ->
        if canvas = __.canvas then
          let _if = If ((tabs.idx((=)__.canvas)) = i)
          btn 
          $ (_if "dff" "fff").bg
          $ (_if "077" "000").fg
          |> ig


    // events
    btnOpen => openFileDialog

    btnDef =+! {
      let! bmp = bmp
      tr.Init
      vCropOffset <- vec 0 0
      r32Crop <- bmp.r32
      do! arrangeBmp true r32Crop }
    
    btnFlip =+! {
      isFlip <- isFlip.n
      tr.Angle -tr.angle
      do! arrangeBmp false r32Crop }

    btnRem =+ { __.canvas.rem __ }

    img.lDragAbs >=< sp.lDragAbs => fun e -> 
      if   isKeyDown Key.C then
        rotateZoom e
      elif isKeyDown Key.X then
        ()
      else
        e.Vec.mv __

    img.rDragAbs => rotateZoom
    
    img.mdown => fun e ->
      bringTop None
      if mPressed || isKeyDown Key.X then
        pDown <- Some e.pos
        rectCrop $ wh 0 0 $ vis |> ig

    wnd.MouseMove => fun e ->
      let p1 = e.posOn img
      if pDown.ok && ( mPressed || isKeyDown Key.X ) then 
        rectCrop |> rect (rectInsideImage pDown.v p1)
    
    wnd.mup =+! {
      let p1 = MPosOn img
      if pDown.ok then
        let! bmp = bmp
        let rect = rectInsideImage pDown.v p1 +. vCropOffset
        let p = rect.TopLeft
        vCropOffset <- vec p.X p.Y
        let! _r32Crop = resizeRect szImage bmp.psz rect 
        r32Crop <- _r32Crop
        do! arrangeBmp true r32Crop
      pDown <- None
      rectCrop |> collapsed }

    gamma =+! { do! arrangeBmp false r32Crop }

    wnd.mleave =+ { pDown <- None }       


    // init
    tryImmediate' {
    if props.ok then 
      match props.v.split ["("; ","; ")"] with
      | [| path; x; y; z; scX; scY; angle; cropX; cropY; cropW; cropH; flip |] -> 
          do! __.openFile (path.rem' "\\\"")
          try' { mv (pfloat x) (pfloat y) __ }
          bringTop (Some (i.Parse z))
          isFlip <- Boolean.Parse flip
          r32Crop <- R32 (i.Parse cropX, i.Parse cropY, i.Parse cropW, i.Parse cropH)
          do! 100
          do! __.canvas.menter.wait
          do! 100
          do! arrangeBmp true r32Crop
          pfloat scX   |> tr.Scale
          pfloat angle |> tr.Rotate
      | [| path |] -> 
          do! __.openFile (path.rem' "\\\"") 
      | _ -> () 
    else
      do! 1
      openFileDialog() }

  member __.canvas : Canvas = __.parent :?> Canvas

  member __.idx = tabs.indexOf __.canvas

  member __.menuVis isVis = sp.Vis' isVis

  member __.openFile file = async {
    _path <- file
    bmp <- 
      if file.urlOk 
      then try_ { return Wb(bimgSafe file) }
      else mkImageSource file 
    match bmp with
    | Some bmp ->
      r32Crop <- bmp.r32
      do! arrangeBmp true r32Crop
      szImage <- img.asz
    | _ -> mf "画像ファイル %s を開くのに失敗しました。" file }

  member __.properties = 
    _path.fileOk.some(
      _path,
      __.po.X, __.po.Y, zIndex, 
      tr.scX, tr.scY, tr.angle, 
      r32Crop.X, r32Crop.Y,
      r32Crop.w, r32Crop.h,
      isFlip
      )

