﻿[<AutoOpen>]
module CompoundCanvas
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Media.Imaging 
open FSharp.Data

type CompoundCanvas(idx) as __ =
  inherit Canvas(AllowDrop=true)
  static let tabs   = rarray<Canvas>()
  let mutable isVis = true
  let pathsImage    = pshs (sf "pathsImage_%d"   idx) ""
  let coBackground  = pshs (sf "coBackground_%d" idx) "0000"
  do
    __ |>* tabs

    loaded =+ {
      for properties in pathsImage.v.split ";" do
        CompoundImage(tabs, __.gamma, props=properties) |>* __ }

    wnd.Closing =+ {
      [ for __ in Seq.cast<CompoundImage> __.cs do 
        if __.properties.ok then
          yield sf "%A" __.properties.v 
      ].concat ";" |>* pathsImage }

    coBackground => fun co -> __ |> co.bg
       
    __.Drop => fun e -> 
      let urls0 = seq {
        let html = e.Data.GetData "HTML Format" :?> s
        if html.ok then
          for attr in (HtmlDocument.Parse html).Descendants "img" |> Seq.choose(HtmlNode.tryGetAttribute "src") do
            yield attr.Value() }
      e.Data.GetData DataFormats.FileDrop |> tryCast |? urls0 |> fun files -> 
        for file in files do
          CompoundImage(tabs, __.gamma, file) |>* __

  member __.menuTransparent = 
    isVis <- isVis.n
    for __ in __.cs do 
      (__ :?> CompoundImage).menuVis isVis

  member __.addImage = CompoundImage(tabs, __.gamma) |>* __

  member __.clearCanvas =
    match MessageBox.Show("キャンバス上に表示されている画像パネルを消去しますか？", "キャンバスクリア", MessageBoxButton.OKCancel) with
    | MessageBoxResult.OK -> __.clear
    | _ -> ()

  member __.saveCanvas =
    let hide = isVis
    if hide then 
      __.menuTransparent
    let rtb = mkRtb __.rsz
    rtb.Render __
    BitmapFrame.Create rtb |> Filer.saveImg
    if hide then 
      __.menuTransparent
       
  member __.setBackground co = 
    coBackground <*- co

  member val gamma = 
    pshf (sf "gamma_%d" idx) 0. with get, set
